﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Nasr.WebPage_v1.Models
{
    public partial class Fix
    {
        public DateTime EffectiveDate { get; set; }
        public string Id { get; set; }
        public double LatDeg { get; set; }
        public double LonDeg { get; set; }
        public int UseType { get; set; }
        public bool? IsController { get; set; }
    }
}
