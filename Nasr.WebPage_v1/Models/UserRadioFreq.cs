﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Nasr.WebPage_v1.Models
{
    public partial class UserRadioFreq
    {
        public string AirportId { get; set; }
        public int FreqMhz { get; set; }
        public int Type { get; set; }

        public virtual UserAirport Airport { get; set; }
    }
}
