﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Nasr.WebPage_v1.Models
{
    public partial class LandingAreaEndpoint
    {
        public DateTime EffectiveDate { get; set; }
        public string AirportId { get; set; }
        public string LandingAreaId { get; set; }
        public string Id { get; set; }
        public bool? IsRightTraffic { get; set; }
        public double LatDeg { get; set; }
        public double LonDeg { get; set; }

        public virtual LandingArea LandingArea { get; set; }
    }
}
