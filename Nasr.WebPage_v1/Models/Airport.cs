﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Nasr.WebPage_v1.Models
{
    public partial class Airport
    {
        public Airport()
        {
            LandingAreas = new HashSet<LandingArea>();
            RadioFreqs = new HashSet<RadioFreq>();
        }

        public DateTime EffectiveDate { get; set; }
        public string Id { get; set; }
        public double LatDeg { get; set; }
        public double LonDeg { get; set; }
        public string Icao { get; set; }
        public string Name { get; set; }
        public bool? IsTowered { get; set; }
        public int FacilityType { get; set; }
        public int Airspace { get; set; }

        public virtual ICollection<LandingArea> LandingAreas { get; set; }
        public virtual ICollection<RadioFreq> RadioFreqs { get; set; }
    }
}
