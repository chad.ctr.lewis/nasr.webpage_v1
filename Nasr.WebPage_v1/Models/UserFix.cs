﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Nasr.WebPage_v1.Models
{
    public partial class UserFix
    {
        public string Id { get; set; }
        public double LatDeg { get; set; }
        public double LonDeg { get; set; }
        public int UseType { get; set; }
        public bool? IsController { get; set; }
    }
}
