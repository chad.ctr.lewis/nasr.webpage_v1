﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Nasr.WebPage_v1.Models
{
    public partial class LandingArea
    {
        public LandingArea()
        {
            LandingAreaEndpoints = new HashSet<LandingAreaEndpoint>();
        }

        public DateTime EffectiveDate { get; set; }
        public string AirportId { get; set; }
        public string Id { get; set; }
        public float LengthFt { get; set; }
        public float WidthFt { get; set; }
        public int Type { get; set; }
        public float ElevationMslFt { get; set; }
        public float HeadingTrueDeg { get; set; }

        public virtual Airport Airport { get; set; }
        public virtual ICollection<LandingAreaEndpoint> LandingAreaEndpoints { get; set; }
    }
}
