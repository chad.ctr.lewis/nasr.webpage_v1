﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Nasr.WebPage_v1.Models
{
    public partial class UserAirport
    {
        public UserAirport()
        {
            UserLandingAreas = new HashSet<UserLandingArea>();
            UserRadioFreqs = new HashSet<UserRadioFreq>();
        }

        public string Id { get; set; }
        public double LatDeg { get; set; }
        public double LonDeg { get; set; }
        public string Icao { get; set; }
        public string Name { get; set; }
        public bool? IsTowered { get; set; }
        public int FacilityType { get; set; }
        public int Airspace { get; set; }

        public virtual ICollection<UserLandingArea> UserLandingAreas { get; set; }
        public virtual ICollection<UserRadioFreq> UserRadioFreqs { get; set; }
    }
}
