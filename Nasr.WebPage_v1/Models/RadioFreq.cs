﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Nasr.WebPage_v1.Models
{
    public partial class RadioFreq
    {
        public DateTime EffectiveDate { get; set; }
        public string AirportId { get; set; }
        public int FreqMhz { get; set; }
        public int Type { get; set; }

        public virtual Airport Airport { get; set; }
    }
}
