﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Nasr.WebPage_v1.Models
{
    public partial class UserLandingArea
    {
        public UserLandingArea()
        {
            UserLandingAreaEndpoints = new HashSet<UserLandingAreaEndpoint>();
        }

        public string AirportId { get; set; }
        public string Id { get; set; }
        public float LengthFt { get; set; }
        public float WidthFt { get; set; }
        public int Type { get; set; }
        public float ElevationMslFt { get; set; }
        public float HeadingTrueDeg { get; set; }

        public virtual UserAirport Airport { get; set; }
        public virtual ICollection<UserLandingAreaEndpoint> UserLandingAreaEndpoints { get; set; }
    }
}
