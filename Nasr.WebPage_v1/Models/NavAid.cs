﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Nasr.WebPage_v1.Models
{
    public partial class NavAid
    {
        public DateTime EffectiveDate { get; set; }
        public string Id { get; set; }
        public double LatDeg { get; set; }
        public double LonDeg { get; set; }
        public string Name { get; set; }
        public int FreqMhz { get; set; }
        public int NavType { get; set; }
        public float ElevationMslFt { get; set; }
        public float MagVarDeg { get; set; }
    }
}
