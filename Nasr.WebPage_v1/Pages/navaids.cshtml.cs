﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Nasr.WebPage_v1.DbContexts;
using Nasr.WebPage_v1.Models;
using System.Collections.Generic;

namespace Nasr.WebPage_v1.Pages
{
    public class NavAidsModel : PageModel
    {

        private NasServerContext db;

        public NavAidsModel(NasServerContext injectedContext)
        {
            db = injectedContext;
        }

        public IEnumerable<NavAid> NavAids { get; set; }

        public void OnGet()
        {
            ViewData["Title"] = "NASR 28-Day Substription Data - Nav Aids";

            NavAids = db.NavAids;
        }
    }
}
