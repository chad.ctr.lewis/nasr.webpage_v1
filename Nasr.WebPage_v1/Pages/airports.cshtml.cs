﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Nasr.WebPage_v1.DbContexts;
using Nasr.WebPage_v1.Models;
using System.Collections.Generic;

namespace Nasr.WebPage_v1.Pages
{
    public class AirportsModel: PageModel
    {

        private NasServerContext db;

        public AirportsModel(NasServerContext injectedContext)
        {
            db = injectedContext;
        }

        public IEnumerable<Airport> Airports { get; set; }

        public void OnGet()
        {
            ViewData["Title"] = "NASR 28-Day Substription Data - Airports";

            Airports = db.Airports;
        }
    }
}