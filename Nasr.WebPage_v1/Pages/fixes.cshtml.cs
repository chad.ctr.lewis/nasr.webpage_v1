﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Nasr.WebPage_v1.DbContexts;
using Nasr.WebPage_v1.Models;
using System.Collections.Generic;

namespace Nasr.WebPage_v1.Pages
{
    public class FixesModel : PageModel
    {

        private NasServerContext db;

        public FixesModel(NasServerContext injectedContext)
        {
            db = injectedContext;
        }

        public IEnumerable<Fix> Fixes { get; set; }

        public void OnGet()
        {
            ViewData["Title"] = "NASR 28-Day Substription Data - Fixes";

            Fixes = db.Fixes;
        }
    }
}
