﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Nasr.WebPage_v1.Models;

#nullable disable

namespace Nasr.WebPage_v1.DbContexts
{
    public partial class NasServerContext : DbContext
    {
        public NasServerContext()
        {
        }

        public NasServerContext(DbContextOptions<NasServerContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Airport> Airports { get; set; }
        public virtual DbSet<Fix> Fixes { get; set; }
        public virtual DbSet<LandingArea> LandingAreas { get; set; }
        public virtual DbSet<LandingAreaEndpoint> LandingAreaEndpoints { get; set; }
        public virtual DbSet<NavAid> NavAids { get; set; }
        public virtual DbSet<RadioFreq> RadioFreqs { get; set; }
        public virtual DbSet<UserAirport> UserAirports { get; set; }
        public virtual DbSet<UserFix> UserFixes { get; set; }
        public virtual DbSet<UserLandingArea> UserLandingAreas { get; set; }
        public virtual DbSet<UserLandingAreaEndpoint> UserLandingAreaEndpoints { get; set; }
        public virtual DbSet<UserNavAid> UserNavAids { get; set; }
        public virtual DbSet<UserRadioFreq> UserRadioFreqs { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Airport>(entity =>
            {
                entity.HasKey(e => new { e.EffectiveDate, e.Id })
                    .IsClustered(false);

                entity.ToTable("Airport");

                entity.Property(e => e.EffectiveDate).HasColumnType("date");

                entity.Property(e => e.Id)
                    .HasMaxLength(64)
                    .HasColumnName("ID");

                entity.Property(e => e.Airspace).HasDefaultValueSql("('0')");

                entity.Property(e => e.FacilityType).HasDefaultValueSql("('0')");

                entity.Property(e => e.Icao)
                    .IsRequired()
                    .HasMaxLength(4)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.IsTowered)
                    .IsRequired()
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.LatDeg).HasDefaultValueSql("('0')");

                entity.Property(e => e.LonDeg).HasDefaultValueSql("('0')");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(64)
                    .HasDefaultValueSql("('New Airport')");
            });

            modelBuilder.Entity<Fix>(entity =>
            {
                entity.HasKey(e => new { e.EffectiveDate, e.Id })
                    .IsClustered(false);

                entity.ToTable("Fix");

                entity.Property(e => e.EffectiveDate).HasColumnType("date");

                entity.Property(e => e.Id)
                    .HasMaxLength(64)
                    .HasColumnName("ID");

                entity.Property(e => e.IsController)
                    .IsRequired()
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.LatDeg).HasDefaultValueSql("('0')");

                entity.Property(e => e.LonDeg).HasDefaultValueSql("('0')");

                entity.Property(e => e.UseType).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<LandingArea>(entity =>
            {
                entity.HasKey(e => new { e.EffectiveDate, e.AirportId, e.Id })
                    .IsClustered(false);

                entity.ToTable("LandingArea");

                entity.Property(e => e.EffectiveDate).HasColumnType("date");

                entity.Property(e => e.AirportId)
                    .HasMaxLength(64)
                    .HasColumnName("AirportID");

                entity.Property(e => e.Id)
                    .HasMaxLength(64)
                    .HasColumnName("ID");

                entity.Property(e => e.ElevationMslFt).HasDefaultValueSql("('0')");

                entity.Property(e => e.HeadingTrueDeg).HasDefaultValueSql("('0')");

                entity.Property(e => e.LengthFt).HasDefaultValueSql("('0')");

                entity.Property(e => e.Type).HasDefaultValueSql("('0')");

                entity.Property(e => e.WidthFt).HasDefaultValueSql("('0')");

                entity.HasOne(d => d.Airport)
                    .WithMany(p => p.LandingAreas)
                    .HasForeignKey(d => new { d.EffectiveDate, d.AirportId })
                    .HasConstraintName("FK_LandingArea");
            });

            modelBuilder.Entity<LandingAreaEndpoint>(entity =>
            {
                entity.HasKey(e => new { e.EffectiveDate, e.AirportId, e.LandingAreaId, e.Id })
                    .IsClustered(false);

                entity.ToTable("LandingAreaEndpoint");

                entity.Property(e => e.EffectiveDate).HasColumnType("date");

                entity.Property(e => e.AirportId)
                    .HasMaxLength(64)
                    .HasColumnName("AirportID");

                entity.Property(e => e.LandingAreaId)
                    .HasMaxLength(64)
                    .HasColumnName("LandingAreaID");

                entity.Property(e => e.Id)
                    .HasMaxLength(64)
                    .HasColumnName("ID");

                entity.Property(e => e.IsRightTraffic)
                    .IsRequired()
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.LatDeg).HasDefaultValueSql("('0')");

                entity.Property(e => e.LonDeg).HasDefaultValueSql("('0')");

                entity.HasOne(d => d.LandingArea)
                    .WithMany(p => p.LandingAreaEndpoints)
                    .HasForeignKey(d => new { d.EffectiveDate, d.AirportId, d.LandingAreaId })
                    .HasConstraintName("FK_LandingAreaEndpoint");
            });

            modelBuilder.Entity<NavAid>(entity =>
            {
                entity.HasKey(e => new { e.EffectiveDate, e.Id })
                    .IsClustered(false);

                entity.ToTable("NavAid");

                entity.Property(e => e.EffectiveDate).HasColumnType("date");

                entity.Property(e => e.Id)
                    .HasMaxLength(64)
                    .HasColumnName("ID");

                entity.Property(e => e.ElevationMslFt).HasDefaultValueSql("('0')");

                entity.Property(e => e.FreqMhz).HasColumnName("FreqMHz");

                entity.Property(e => e.LatDeg).HasDefaultValueSql("('0')");

                entity.Property(e => e.LonDeg).HasDefaultValueSql("('0')");

                entity.Property(e => e.MagVarDeg).HasDefaultValueSql("('0')");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(64)
                    .HasDefaultValueSql("('New NAV Aid')");

                entity.Property(e => e.NavType).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<RadioFreq>(entity =>
            {
                entity.HasKey(e => new { e.EffectiveDate, e.AirportId, e.FreqMhz, e.Type })
                    .IsClustered(false);

                entity.ToTable("RadioFreq");

                entity.Property(e => e.EffectiveDate).HasColumnType("date");

                entity.Property(e => e.AirportId)
                    .HasMaxLength(64)
                    .HasColumnName("AirportID");

                entity.Property(e => e.FreqMhz).HasColumnName("FreqMHz");

                entity.Property(e => e.Type).HasDefaultValueSql("('0')");

                entity.HasOne(d => d.Airport)
                    .WithMany(p => p.RadioFreqs)
                    .HasForeignKey(d => new { d.EffectiveDate, d.AirportId })
                    .HasConstraintName("FK_RadioFreq");
            });

            modelBuilder.Entity<UserAirport>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .IsClustered(false);

                entity.ToTable("UserAirport");

                entity.Property(e => e.Id)
                    .HasMaxLength(64)
                    .HasColumnName("ID");

                entity.Property(e => e.Airspace).HasDefaultValueSql("('0')");

                entity.Property(e => e.FacilityType).HasDefaultValueSql("('0')");

                entity.Property(e => e.Icao)
                    .IsRequired()
                    .HasMaxLength(4)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.IsTowered)
                    .IsRequired()
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.LatDeg).HasDefaultValueSql("('0')");

                entity.Property(e => e.LonDeg).HasDefaultValueSql("('0')");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(64)
                    .HasDefaultValueSql("('New Airport')");
            });

            modelBuilder.Entity<UserFix>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .IsClustered(false);

                entity.ToTable("UserFix");

                entity.Property(e => e.Id)
                    .HasMaxLength(64)
                    .HasColumnName("ID");

                entity.Property(e => e.IsController)
                    .IsRequired()
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.LatDeg).HasDefaultValueSql("('0')");

                entity.Property(e => e.LonDeg).HasDefaultValueSql("('0')");

                entity.Property(e => e.UseType).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<UserLandingArea>(entity =>
            {
                entity.HasKey(e => new { e.AirportId, e.Id })
                    .IsClustered(false);

                entity.ToTable("UserLandingArea");

                entity.Property(e => e.AirportId)
                    .HasMaxLength(64)
                    .HasColumnName("AirportID");

                entity.Property(e => e.Id)
                    .HasMaxLength(64)
                    .HasColumnName("ID");

                entity.Property(e => e.ElevationMslFt).HasDefaultValueSql("('0')");

                entity.Property(e => e.HeadingTrueDeg).HasDefaultValueSql("('0')");

                entity.Property(e => e.LengthFt).HasDefaultValueSql("('0')");

                entity.Property(e => e.Type).HasDefaultValueSql("('0')");

                entity.Property(e => e.WidthFt).HasDefaultValueSql("('0')");

                entity.HasOne(d => d.Airport)
                    .WithMany(p => p.UserLandingAreas)
                    .HasForeignKey(d => d.AirportId)
                    .HasConstraintName("FK_UserLandingArea");
            });

            modelBuilder.Entity<UserLandingAreaEndpoint>(entity =>
            {
                entity.HasKey(e => new { e.AirportId, e.LandingAreaId, e.Id })
                    .IsClustered(false);

                entity.ToTable("UserLandingAreaEndpoint");

                entity.Property(e => e.AirportId)
                    .HasMaxLength(64)
                    .HasColumnName("AirportID");

                entity.Property(e => e.LandingAreaId)
                    .HasMaxLength(64)
                    .HasColumnName("LandingAreaID");

                entity.Property(e => e.Id)
                    .HasMaxLength(64)
                    .HasColumnName("ID");

                entity.Property(e => e.IsRightTraffic)
                    .IsRequired()
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.LatDeg).HasDefaultValueSql("('0')");

                entity.Property(e => e.LonDeg).HasDefaultValueSql("('0')");

                entity.HasOne(d => d.UserLandingArea)
                    .WithMany(p => p.UserLandingAreaEndpoints)
                    .HasForeignKey(d => new { d.AirportId, d.LandingAreaId })
                    .HasConstraintName("FK_UserLandingAreaEndpoint");
            });

            modelBuilder.Entity<UserNavAid>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .IsClustered(false);

                entity.ToTable("UserNavAid");

                entity.Property(e => e.Id)
                    .HasMaxLength(64)
                    .HasColumnName("ID");

                entity.Property(e => e.ElevationMslFt).HasDefaultValueSql("('0')");

                entity.Property(e => e.FreqMhz).HasColumnName("FreqMHz");

                entity.Property(e => e.LatDeg).HasDefaultValueSql("('0')");

                entity.Property(e => e.LonDeg).HasDefaultValueSql("('0')");

                entity.Property(e => e.MagVarDeg).HasDefaultValueSql("('0')");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(64)
                    .HasDefaultValueSql("('New NAV Aid')");

                entity.Property(e => e.NavType).HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<UserRadioFreq>(entity =>
            {
                entity.HasKey(e => new { e.AirportId, e.FreqMhz, e.Type })
                    .IsClustered(false);

                entity.ToTable("UserRadioFreq");

                entity.Property(e => e.AirportId)
                    .HasMaxLength(64)
                    .HasColumnName("AirportID");

                entity.Property(e => e.FreqMhz).HasColumnName("FreqMHz");

                entity.Property(e => e.Type).HasDefaultValueSql("('0')");

                entity.HasOne(d => d.Airport)
                    .WithMany(p => p.UserRadioFreqs)
                    .HasForeignKey(d => d.AirportId)
                    .HasConstraintName("FK_UserRadioFreq");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
